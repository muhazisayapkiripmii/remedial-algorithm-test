//   - `anagram('aaz', 'zza')` => false
//   - `anagram('anagram', 'nagaram'))` => true

function anagram (arr1,arr2) {
    let a = {};

    if (arr1.length !== arr2.length) {
        return false;
    }
    for (let i = 0; i < arr1.length; i++) {
        if (!a[arr1[i]] ) {
            a[arr1[i]] = 1;
        } else {
            a[arr1[i]] = a[arr1[i]] + 1; 
        }
    }
    for (let i = 0; i < arr2.length; i++) {
        if (!a[arr2[i]]) {
            return false;
        }
        a[arr2[i]]--;
    }
    return true;
}
console.log(anagram("aaz","zza"));
console.log(anagram("anagram","nagaram"));